#%%
import collections
from multiprocessing.sharedctypes import Value
import gym
import numpy as np
import statistics
import tensorflow as tf
import tensorflow_probability as tfp
import tqdm

import os
from matplotlib import pyplot as plt, scale
from tensorflow.keras import layers
from typing import Any, List, Sequence, Tuple

import time 



gpus = tf.config.list_physical_devices('GPU')
if gpus:
    try:
        tf.config.set_logical_device_configuration(gpus[0],[tf.config.LogicalDeviceConfiguration(memory_limit=2048)])
        logical_gpus = tf.config.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Virtual devices must be set before GPUs have been initialized
        print(e)


#%% Create the environment
env = gym.make("LunarLanderContinuous-v2")

#%% Set seed for experiment reproducibility
seed = 42
env.seed(seed)
tf.random.set_seed(seed)
np.random.seed(seed)

min_episodes_criterion = 100
max_episodes = 100000
max_steps_per_episode = 1000
batch = 10

# consecutive trials
reward_threshold = -10
running_reward = 0
# Discount factor for future rewards
gamma = 0.99

#lr以PPO重複訓練幾次降低
train_loop_time = 10
# lr  = 0.0001/train_loop_time
lr  = 0.001/train_loop_time


# Small epsilon value for stabilizing division operations
EPSILON = 0.2
eps = np.finfo(np.float32).eps.item()
class PPO(tf.keras.Model):
  """Combined actor-critic network."""

  def __init__(
      self, 
      num_actions: int, 
      num_hidden_units: int):
    """Initialize."""
    super().__init__()

    self.common_1 = layers.Dense(num_hidden_units*2, activation="relu",name = "common_1")
    self.common_2 = layers.Dense(num_hidden_units, activation="relu",name = "common_2")
    self.actor_mu = layers.Dense(num_actions, activation="tanh",name = "actor_mu")
    self.critic = layers.Dense(1,name = "critic")

    self.old_common_1 = layers.Dense(num_hidden_units*2, activation="relu",trainable=False,name = "old_common_1")
    self.old_common_2 = layers.Dense(num_hidden_units, activation="relu",trainable=False,name = "old_common_2")
    self.old_actor_mu = layers.Dense(num_actions, activation="tanh",trainable=False,name = "old_actor_mu")
    self.old_critic = layers.Dense(1,trainable=False,name = "old_critic")

  def call(self, inputs: tf.Tensor) -> Tuple[tfp.distributions.Normal,tfp.distributions.Normal,tf.Tensor, tf.Tensor]:
    x = self.common_1(inputs)
    x = self.common_2(x)
    x_ = self.old_common_1(inputs)
    x_ = self.old_common_2(x_)


    return self.actor_mu(x) , self.old_actor_mu(x_), self.critic(x) , self.old_critic(x_)
  

#%%

num_actions = env.action_space.shape[0]  # [-1. -1.], [1. 1.], (2,) float32

num_hidden_units = 200

model = PPO(num_actions, num_hidden_units)

#%%
def env_step(action: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
  """Returns state, reward and done flag given an action."""

  state, reward, done, _ = env.step(action)
  return (state.astype(np.float32), 
          np.array(reward, np.int32), 
          np.array(done, np.int32))


def tf_env_step(action: tf.Tensor) -> List[tf.Tensor]:
  return tf.numpy_function(env_step, [action], 
                           [tf.float32, tf.int32, tf.int32])


def run_episode(
    initial_state: tf.Tensor,  
    model: tf.keras.Model, 
    max_steps: int) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
  """Runs a single episode to collect training data."""

  old_action_probs = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
  old_actions = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
  values = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
  rewards = tf.TensorArray(dtype=tf.int32, size=0, dynamic_size=True)
  states = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
  initial_state_shape = initial_state.shape
  state = initial_state

  for t in tf.range(max_steps):
    # Convert state into a batched tensor (batch size = 1)
    states = states.write(t,tf.convert_to_tensor(state))

    state = tf.expand_dims(state, 0)
    # Run the model and to get action probabilities and critic value
    action_t,old_action_t, _ , old_value= model(state)

    # Sample next action from the action probability distribution

    
    old_action = tf.clip_by_value(old_action_t,clip_value_min=-1,clip_value_max=1) #選取的動作

    old_action_probs_t = old_action_t.prob(old_action) #選取動作的機率
    # Store critic values
    values = values.write(t, tf.squeeze(old_value)) #預估的reword期望值
    # Store log probability of the action chosen
    old_action_probs = old_action_probs.write(t, old_action_probs_t)

    old_actions = old_actions.write(t,tf.squeeze(old_action))

    # Apply action to the environment to get next state and reward
    state, reward, done = tf_env_step(old_action)
    state.set_shape(initial_state_shape)


    # Store reward
    rewards = rewards.write(t, reward)

    if tf.cast(done, tf.bool):
      break


  old_action_probs = old_action_probs.stack()
  old_actions = old_actions.stack()
  values = values.stack()
  rewards = rewards.stack()
  states = states.stack()
  return old_action_probs, old_actions, values, rewards, states

def run_new_obs(states: tf.Tensor,
    old_actions: tf.Tensor,  
    model: tf.keras.Model,) -> Tuple[tf.Tensor ,tf.Tensor ,tf.Tensor]:

    action_t, _, value , old_value = model(states)

    # Sample next action from the action probability distribution
    # acts = tf.squeeze((tf.random.categorical(action_t, 1)))   ddd
    action_probs_t = action_t.prob(old_actions)
    return action_probs_t, value ,old_value
    

def get_expected_return(
    rewards: tf.Tensor, 
    gamma: float, 
    standardize: bool = True) -> tf.Tensor:
  """Compute expected returns per timestep."""

  n = tf.shape(rewards)[0]
  returns = tf.TensorArray(dtype=tf.float32, size=n)

  # Start from the end of `rewards` and accumulate reward sums
  # into the `returns` array
  rewards = tf.cast(rewards[::-1], dtype=tf.float32)
  discounted_sum = tf.constant(0.0)
  discounted_sum_shape = discounted_sum.shape
  for i in tf.range(n):
    reward = rewards[i]
    discounted_sum = reward + gamma * discounted_sum
    discounted_sum.set_shape(discounted_sum_shape)
    returns = returns.write(i, discounted_sum)
  returns = returns.stack()[::-1]

  if standardize:
    returns = ((returns - tf.math.reduce_mean(returns)) / 
               (tf.math.reduce_std(returns) + eps))

  return returns

huber_loss = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.SUM)

def compute_loss(
    action_probs: tf.Tensor,  
    old_action_probs: tf.Tensor, 
    values: tf.Tensor,  
    old_values: tf.Tensor,  
    returns: tf.Tensor) -> tf.Tensor:
  """Computes the combined actor-critic loss."""

  advantage = returns - old_values

  ration = tf.convert_to_tensor(action_probs / (old_action_probs + eps))
  
  # print(action_probs[0],old_action_probs[0],advantage[0])

  ration_clip = tf.clip_by_value(ration, 1.-EPSILON, 1.+EPSILON)

  actor_loss = -tf.math.reduce_mean(tf.math.minimum(ration  * advantage, ration_clip  * advantage))


  critic_loss = huber_loss(values, returns)
  # print(actor_loss , critic_loss)
  return actor_loss , critic_loss


optimizer_a = tf.keras.optimizers.Adam(learning_rate= lr)
optimizer_c = tf.keras.optimizers.Adam(learning_rate= lr/1000)

@tf.function
def train_step(
    initial_state: tf.Tensor, 
    model: tf.keras.Model, 
    optimizer_a: tf.keras.optimizers.Optimizer, 
    optimizer_c: tf.keras.optimizers.Optimizer, 
    gamma: float, 
    max_steps_per_episode: int) -> tf.Tensor:
  """Runs a model training step."""
  # Run the model for one episode to collect training data
  old_action_probs, old_actions, values, rewards, states = run_episode(
      initial_state, model, max_steps_per_episode) 

  # Calculate expected returns
  returns = get_expected_return(rewards, gamma)
  # returns = tf.cast(rewards/200, dtype=tf.float32)
  

  old_action_probs, returns = [
    tf.expand_dims(x, 1) for x in [old_action_probs, returns]]
  episode_reward = 0 
  for _ in range(train_loop_time):
    with tf.GradientTape(persistent=True) as tape:
      
      # Convert training data to appropriate TF tensor shapes
      action_probs ,values , old_values= run_new_obs(states, old_actions, model)
      action_probs = tf.expand_dims(action_probs,1)

      # Calculating loss values to update our network
      loss_a,loss_c = compute_loss(action_probs, old_action_probs, values, old_values, returns)
      # Compute the gradients from the loss

    grads_a = tape.gradient(loss_a, model.trainable_variables[:8])
    grads_c = tape.gradient(loss_c, model.trainable_variables[:4]+model.trainable_variables[8:])
    # Apply the gradients to the model's parameters

    optimizer_a.apply_gradients(zip(grads_a, model.trainable_variables[:8]))
    optimizer_c.apply_gradients(zip(grads_c, model.trainable_variables[:4]+model.trainable_variables[8:]))

  episode_reward = tf.math.reduce_sum(rewards)

  return episode_reward



# Keep last episodes reward
episodes_reward: collections.deque = collections.deque(maxlen=min_episodes_criterion)

def change_weight(model: tf.keras.Model
  ) -> tf.keras.Model:

  model.old_common_1.set_weights(model.common_1.get_weights())
  model.old_common_2.set_weights(model.common_2.get_weights())
  model.old_actor_mu.set_weights(model.actor_mu.get_weights())
  model.old_actor_sigma.set_weights(model.actor_sigma.get_weights())
  model.old_critic.set_weights(model.critic.get_weights())
  return model

with tqdm.trange(max_episodes) as t:
  for i in t:
    initial_state = tf.constant(env.reset(), dtype=tf.float32)
    model = change_weight(model)
    episode_reward = int(train_step(
        initial_state, model, optimizer_a,optimizer_c, gamma, max_steps_per_episode))
    episodes_reward.append(episode_reward)
    running_reward = statistics.mean(episodes_reward)

    t.set_description(f'Episode {i}')
    t.set_postfix(
        episode_reward=episode_reward, running_reward=running_reward)

    # Show average episode reward every 10 episodes
    if i % 10 == 0:
      pass # print(f'Episode {i}: average reward: {avg_reward}')

    if running_reward > reward_threshold and i >= min_episodes_criterion: 
        print("done")
        break

print(f'\nSolved at episode {i}: average reward: {running_reward:.2f}!')



# Render an episode and save as a GIF file
#%%
from IPython import display as ipythondisplay
from PIL import Image
from pyvirtualdisplay import Display


# display = Display(visible=0, size=(400, 300))
# display.start()

#%%
def render_episode(env: gym.Env, model: tf.keras.Model, max_steps: int): 
  screen = env.render(mode='rgb_array')
  im = Image.fromarray(screen)

  images = [im]

  state = tf.constant(env.reset(), dtype=tf.float32)
  for i in range(1, max_steps + 1):
    state = tf.expand_dims(state, 0)
    action_probs, _, _ , _ = model(state)
    action = tf.clip_by_value(action_probs.sample(),clip_value_min=-1,clip_value_max=1)
    print(action_probs.prob(action))
    state, _, done, _ = env.step(action)
    state = tf.constant(state, dtype=tf.float32)

    # Render screen every 10 steps
    if i % 10 == 0:
      screen = env.render(mode='rgb_array')
      images.append(Image.fromarray(screen))

    if done:
      break

  return images

#%%
# Save GIF image 
images = render_episode(env, model, max_steps_per_episode)
#%%
image_file = 'LunarLanderContinuous_000.gif'
# loop=0: loop forever, duration=1: play each frame for 1ms
images[0].save(
    image_file, save_all=True, append_images=images[1:], loop=0, duration=0.01)


# %%
